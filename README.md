# 生存確認ツール

〜コミュニケーションの問題をテクノロジーで解決し太郎シリーズ〜

## これは何

孤独死が社会的な問題になっています。このツールは、一人暮らしをする人間用の簡易デッドマンシステム(生きていることが確認できない場合通報する仕組み)です。Amazon Web Services (AWS)のLambda, CloudWatch Events, API Gateway を用いることで完全サーバレスなサービスとして運用可能に仕上げています。

クライアントはWeb で動作するSingle-Page Application形式のものを同梱していますが、API Gatewayに向けてREST API形式のリクエストを投げるだけなので、例えばマイコンにセンサをつけてトリガしても良いと思います。あるいは、AWS IoT Buttonが日本でも発売されれば、ここから直接LambdaをトリガすることでAPI Gatewayは不要になります。

通報部分はSlackのWebhookを叩くようにしていますが、状況によってはAWS Simple Notification Service (SNS)が便利かもしれません。ここは各自工夫してください。

## デプロイ

自動化してない。

* まず通報用のLambdaを作る
  * notifier.js の内容でLambdaを作る
  * notifier.js の冒頭のSlack hookのURLは自分のSlackで作ったIncoming Webhookの情報に合わせる
  * このLambdaのARN をメモしておく
* 生存確認受付用のLambdaを作る
  * receiver.js の内容でLambdaを作る
  * 冒頭eventRuleName は自分が使っている他のサービスのCloudWatch EventsのRule名と重複しなければ何でもOK
  * notifierArnは前ステップでメモしたnotifier.jsに対応するARNを記入
  * secretToken はユーザに配る秘密のトークン。URL-safeでランダムな文字列が良さそう。
* API Gatewayでreceiver.jsに対応するLambdaのエントリポイントを作る
  * メソッドPOSTで、CORSを有効にするのを忘れないようにする
  * CORSを有効にすると自動でOPTIONS メソッドも有効になるはずなので確認する
* S3にウェブクライアントをアップロードする
  * alive.js の冒頭にAPI URLがあるので、ここをAPI Gatewayでデプロイしたあとに出てくるURLに書き換える
    * index.html にもDNS プリフェッチ用にURLがあるのでここもできれば書き換える
  * AWS CLIが動作するように設定しているなら upload-webclient.sh のような感じでアップロード可能
* 監視したい人にURL とREST APIの叩き方を説明する
  * S3のポリシーを適切に設定すればWebsite Endpointを有効にする必要はない
  * `https://s3-ap-region-1.amazonaws.com/your-bucket-name/index.html#secret-token`
    * `#` のあとに先ほど設定した秘密のトークンを追加するのを忘れずに

簡単でしょう？


