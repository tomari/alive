(function(exports) {
	'use strict';
	// CloudWatch Event Rule Name
	const eventRuleName='alive_rule1';
	// ARN for the lambda function of notifier.js 
	const notifierArn='arn:aws:lambda:region-1:user-id:function:functionname';
	// secret token for identifying user
	const secretToken = 'secret-token';
	// rule target assignment id, any string is fine
	const assignmentId="javadownload";

	// Following function is required only for the first invocation
	function setRuleTarget(cwevents, callback) {
		cwevents.putTargets({
			'Rule': eventRuleName,
			'Targets': [{
				'Arn':notifierArn,
				'Id':assignmentId
			}]
		}, (err,data)=>{
			if(err) { callback(null,{'error_code':3,'extra':err.stack}); }
			else {
				callback(null,{'error_code':0});
			}
		});
	}
	function cronExpressionForDate(d) {
		return 'cron('+d.getUTCMinutes()+' '+
				d.getUTCHours()+' '+
				d.getUTCDate()+' '+
				(1+d.getUTCMonth())+' ? '+
				d.getUTCFullYear()+
			')';
	}
	exports.handler=(event,context,callback)=>{
		if(event.token !== secretToken) {
			callback(null, {'error_code':1});
		}
		var notifyDate=new Date();
		notifyDate.setDate(notifyDate.getDate()+1);
		notifyDate.setHours(notifyDate.getHours()+6);
		var cronExpression=cronExpressionForDate(notifyDate);
		var AWS = require('aws-sdk');
		var cwevents = new AWS.CloudWatchEvents({apiVersion: '2015-10-07'});
		cwevents.putRule({
			'Name': eventRuleName,
			'Description': 'Alive automatic rule',
			'ScheduleExpression': cronExpression,
			'State': 'ENABLED'
		}, (err,data)=> {
			if(err) { callback(null,{'error_code':2,'extra':err.stack}); }
			else {
				// First invocation
				setRuleTarget(cwevents,callback);
				// After the first invocation
				// callback(null,{'error_code':0});
			}
		});
	};
})(exports);

// ----
//exports.handler({'token':'Y6tVMXSZGAcws'},null,(a,b)=>{console.log(JSON.stringify(b));});

