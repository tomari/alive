(function(exports) {
	'use strict';
	// Slack configuration
	const slack_hook_hostname='hooks.slack.com';
	const slack_hook_port=443;
	const slack_hook_path='/services/aaaaa/bbbbb';
	// Message for dead-man detection
	const message='生存確認ボタンが押されていません';
	exports.handler=(event,context,callback)=>{
		const https=require('https');
		const escapes={
			'<': '&lt;',
			'>': '&gt;',
			'&': '&amp;'
		};
		var msg_esc=message.replace(/[<>&]/g,(m)=>{ return escapes[m]; });
		var slackPostData=JSON.stringify({
			'text':msg_esc
		});
		var req_options={
			'hostname': slack_hook_hostname,
			'port': 443,
			'path': slack_hook_path,
			'method': 'POST',
			'headers': {
				'Content-Type': 'application/json',
				'Content-Length': Buffer.byteLength(slackPostData)
			}
		};
		var req=https.request(req_options,(res)=>{
			if(res.statusCode == 200) {
				callback(null, {'error_code':0});
			} else {
				callback(null, {'error_code':res.statusCode});
			}
		});
		req.on('error',(e)=>{
			callback(null,{'error_code':1, 'extra':e});
		});
		req.write(slackPostData);
		req.end();
	};
})(exports);
//exports.handler(null,null,(a,b)=>{console.log(JSON.stringify(b));});

