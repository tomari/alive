#!/bin/sh
BUCKET_NAME=bucket-name
SRC_DIR=webclient
set -e
p(){
	gzip -c "$SRC_DIR"/"$1" | aws s3 cp - "s3://${BUCKET_NAME}/$1" --content-type "$2" --cache-control "$3" --content-encoding gzip
}
p "alive.css"	"text/css;charset=utf-8"	"public,max-age=31536000"
p "alive.js"	"application/javascript"	"public,max-age=31536000"
p "index.html"	"text/html;charset=utf-8"	"public,max-age=31536000"
