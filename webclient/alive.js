(function(window) {
	'use strict';
	var api_url='https://apigwkey.execute-api.region-1.amazonaws.com/stage/name';
	//
	function View() {
		this.screens=['scrbutton','scrwait','scrres'];
		this.errors=['errtok','errcomm','errsvr'];
	}
	View.prototype.setupButtonPushListener=function(lmd) {
		this.aliveButton=document.getElementById('alivebutton');
		this.aliveButton.addEventListener('click',lmd);
	};
	View.prototype.gotoScreen=function(scrname) {
		this.showOneElem(this.screens,scrname);
	};
	View.prototype.showError=function(errname) {
		this.gotoScreen('scrbutton');
		this.showOneElem(this.errors,errname);
	}
	View.prototype.showOneElem=function(elems,idx){
		elems.forEach(function(e){
			var ee=document.getElementById(e);
			ee.style.display=(e===idx)?'inherit':'none';
		})
	};
	View.prototype.getToken=function() {
		return window.location.hash.slice(1);
	};
	//
	function Controller() {
		this.view=new View();
	}
	Controller.prototype.setupListeners=function() {
		this.view.setupButtonPushListener(this.aliveClicked.bind(this));
	};
	Controller.prototype.aliveClicked=function() {
		this.view.gotoScreen('scrwait');
		var tok=this.view.getToken();
		if(tok.length<4) {
			this.view.showError('errtok');
			return;
		}
		var xhr=new XMLHttpRequest();
		xhr.addEventListener('load',function(evt) {
			this.reportDone(evt,xhr);
		}.bind(this));
		xhr.addEventListener('error',function(evt) {
			this.view.showError('errcomm');
		}.bind(this));
		xhr.open('post',api_url);
		xhr.setRequestHeader('Content-Type','application/json');
		xhr.send(JSON.stringify({
			'token': tok
		}));
	};
	Controller.prototype.reportDone=function(evt,xhr) {
		var res_json=JSON.parse(xhr.responseText);
		var res_code=res_json['error_code'];
		if(res_code===0) {
			this.view.gotoScreen('scrres');
		} else {
			this.view.showError('errsvr');
		}
	};
	//
	window.onload=function() {
		var c=new Controller();
		c.setupListeners();
	};
})(window);

